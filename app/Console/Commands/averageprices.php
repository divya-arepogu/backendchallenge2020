<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
class averageprices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    $dbt = DB::table('ap_copi')
            ->join('ap_states', 'ap_copi.state_id', '=', 'ap_states.id')
            ->selectRaw('ap_states.state as state_name,avg(steak) as steak, avg(grnd_beef) as grnd_beef, avg(sausage) as sausage, avg(fry_chick) as fry_chick, avg(tuna) as tuna, avg(hgal_milk) as hgal_milk, avg(dozen_eggs) as dozen_eggs, avg(margarine) as margarine, avg(parmesan) as parmesan, avg(potatoes) as potatoes, avg(bananas) as bananas, avg(lettuce) as lettuce, avg(bread) as bread, avg(orange_juice) as orange_juice, avg(coffee) as coffee, avg(sugar) as sugar, avg(cereal) as cereal, avg(sweet_peas) as sweet_peas, avg(peaches) as peaches, avg(klnx) as klnx, avg(`cascade`) as `cascade`, avg(cooking_oil) as cooking_oil, avg(frozn_meal) as frozn_meal, avg(frozn_corn) as frozn_corn, avg(potato_chips) as potato_chips, avg(coke) as coke, avg(apt_rent) as apt_rent, avg(home_price) as home_price, avg(mort_rate) as mort_rate, avg(home_pi) as home_pi, avg(all_elect) as all_elect, avg(part_elect) as part_elect, avg(other_energy) as other_energy, avg(total_energy) as total_energy, avg(phone) as phone, avg(tire_bal) as tire_bal, avg(gasoline) as gasoline, avg(optometrist) as optometrist, avg(doctor) as doctor, avg(dentist) as dentist, avg(ibuprofen) as ibuprofen, avg(prescription_drug) as prescription_drug, avg(hmbgr_sand) as hmbgr_sand, avg(pizza) as pizza, avg(2_pc_chick) as pc_chick, avg(hair_cut) as hair_cut, avg(beaut_salon) as beaut_salon, avg(toothpaste) as toothpaste, avg(shampoo) as shampoo, avg(dry_clean) as dry_clean, avg(mens_shirt) as mens_shirt, avg(boys_jeans) as boys_jeans, avg(womens_slacks) as womens_slacks, avg(washr_repr) as washr_repr, avg(newspaper) as newspaper, avg(movie) as movie, avg(yoga) as yoga, avg(tenns_balls) as tenns_balls, avg(vet_services) as vet_services, avg(beer) as beer, avg(wine) as wine')
            ->groupBy('ap_states.state')
            ->get();
        $averages= $dbt->toArray();

        //echo "\n\n"
     foreach ($averages as $average) {
echo "$average->state_name : steak($average->steak), grnd_beef($average->grnd_beef), sausage($average->sausage), fry_chick($average->fry_chick), tuna($average->tuna), hgal_milk($average->hgal_milk), dozen_eggs($average->dozen_eggs), margarine($average->margarine), parmesan($average->parmesan), potatoes($average->potatoes), bananas($average->bananas), lettuce($average->lettuce), bread($average->bread), orange_juice($average->orange_juice), coffee($average->coffee), sugar($average->sugar), cereal($average->cereal), sweet_peas($average->sweet_peas), peaches($average->peaches), klnx($average->klnx), cascade($average->cascade), cooking_oil($average->cooking_oil), frozn_meal($average->frozn_meal), frozn_corn($average->frozn_corn), potato_chips($average->potato_chips), coke($average->coke), apt_rent($average->apt_rent), home_price($average->home_price), mort_rate($average->mort_rate), home_pi($average->home_pi), all_elect($average->all_elect), part_elect($average->part_elect), other_energy($average->other_energy), total_energy($average->total_energy), phone($average->phone), tire_bal($average->tire_bal), gasoline($average->gasoline), optometrist($average->optometrist), doctor($average->doctor), dentist($average->dentist), ibuprofen($average->ibuprofen), prescription_drug($average->prescription_drug), hmbgr_sand($average->hmbgr_sand), pizza($average->pizza), 2_pc_chick($average->pc_chick), hair_cut($average->hair_cut), beaut_salon($average->beaut_salon), toothpaste($average->toothpaste), shampoo($average->shampoo), dry_clean($average->dry_clean), mens_shirt($average->mens_shirt), boys_jeans($average->boys_jeans), womens_slacks($average->womens_slacks), washr_repr($average->washr_repr), newspaper($average->newspaper), movie($average->movie), yoga($average->yoga), tenns_balls($average->tenns_balls), vet_services($average->vet_services), beer($average->beer), wine($average->wine)", PHP_EOL;


         }
            
    }
}
