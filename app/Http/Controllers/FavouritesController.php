<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favourites;
use DB;
class FavouritesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function index()
    {
        $favourites=DB::table('favourites')->where('user_id', auth()->user()->id)->latest()->get();
        return view('favourites.index',['quotes'=>$favourites]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('favourites.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                //
         $request->validate([
          'showname' => 'required',
          'seasonname' => 'required',
          'quote' => 'required',
        ]); 
        
        $favourite= new Favourites();
        $favourite->user_id=auth()->user()->id;
        $favourite->show_name=$request->get('showname');
        $favourite->season_name=$request->get('seasonname');
        $favourite->quote=$request->get('quote');

        $favourite->save();
        return redirect('favourites')->with('success', 'quote has been added to favourites');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $favourite = Favourites::find($id);
        return view('favourites.edit',compact('favourite','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'showname' => 'required',
          'seasonname' => 'required',
          'quote' => 'required',
        ]); 
        
        $favourite = Favourites::find($id);
        $favourite->user_id=auth()->user()->id;
        $favourite->show_name=$request->get('showname');
        $favourite->season_name=$request->get('seasonname');
        $favourite->quote=$request->get('quote');

        $favourite->save();
        return redirect('favourites')->with('success', 'quote has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $favourite = Favourites::find($id);
        $favourite->delete();
        return redirect('favourites')->with('success','quote has been  deleted');
    }
}
