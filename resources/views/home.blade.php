@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-5 p-5"><h2>Recent Favourite Quotes</h2></div>
        <div class=" p-5 d-flex">
            <div  ><a href="favourites" class="btn btn-warning">View All Quotes</a></div>
        <div  style="padding-right: 10px"> </div>
    <div  ><a href="favourites/create" class="btn btn-warning">[+]Add New Quote</a></div></div>
</div>
@if(count($quotes) > 0)
@foreach($quotes as $key => $data)
    <div class="row" style="border-style: groove;border-radius: 15px">
        <div class="col-2 p-2">
            <img src="https://picsum.photos/100/100?random={{$data->id}}" class="rounded-circle">
        </div>
        <div class="col-5  p-2">
            
            <div style="align-items: center">
                <div>{{$data->quote}}</div>
                <div class="d-flex">
                    <div>{{$data->season_name}}, </div>
                    <div>{{$data->show_name}}</div>    
                </div>
            </div>

        </div>
    </div>
    <br>

@endforeach
@else
No favourites are present in database
<a href="favourites/create"><h2>Click here to add a new quote to database</h2></a>
@endif




</div>
@endsection
