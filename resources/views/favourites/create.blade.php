<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Favourite Quotes </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Add A Quote</h2><br  />
      <form method="post" action="{{url('favourites')}}">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="ShowName">ShowName:</label>
            <input type="text" class="form-control" name="showname">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="SeasonName">SeasonName:</label>
              <input type="text" class="form-control" name="seasonname">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Quote">Quote:</label>
              <input type="text" class="form-control" name="quote">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <a href="../favourites" class="btn btn-warning">Cancle</a>
            <button type="submit" class="btn btn-success" style="margin-left:38px">Save Quote</button>
          </div>

           

        </div>
      </form>
    </div>
  </body>
</html>