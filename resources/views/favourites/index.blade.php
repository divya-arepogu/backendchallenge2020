@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-5 p-5"><h1>Favourite Quotes</h1></div>
    <div class="col-5  p-5">
    <div  ><a href="favourites/create" class="btn btn-warning">[+]Add New Quote</a></div></div>
</div>
@if(count($quotes) > 0)
@foreach($quotes->chunk(3) as $chunk)

    <div class="row" >
        @foreach($chunk as $data)
        <div class="col-md-4" style="border-style: groove;border-radius: 15px">
            <div class="row" >
        <div class="col-4 p-2">
            <img src="https://picsum.photos/100/100?random={{$data->id}}" class="rounded-circle">
        </div>
        <div class="col-5  p-2">
            
            <div style="align-items: center;word-wrap: break-word;">
                <div><span>"{{$data->quote}}"</span></div>
                <div>
                    <div><b>{{$data->show_name}}</b></div>
                    <div>({{$data->season_name}})</div>    
                </div>
 
            </div>
<div class="d-flex" >
        <div style="padding-right:5px; "><a href="{{action('FavouritesController@edit', $data->id)}}" class="btn btn-warning">Edit</a></div>
        <div>
          <form action="{{action('FavouritesController@destroy', $data->id)}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </div>
</div>

        </div>
    </div>
    </div>
        @endforeach
    </div>
    <br>

@endforeach
@else
No favourites are present in database
<a href="favourites/create"><h2>Click here to add a new quote to database</h2></a>
@endif



</div>
@endsection
